import express from 'express';
import { Request,Response, NextFunction } from 'express';
const router = express.Router();
router.use(express.json())


router.use((req,res,next) =>{
    console.log("i'm a middleware function in router");
    next();
})



/**
 * generate html string with firstName and lastName
 * @param firstName 
 * @param lastName 
 * @returns 
 */
 function getNameInHTML(firstName : string,lastName :string){
    return ` 
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>

    <p>Welcome To Express:</p>
<ul>
    <li>first name: ${firstName}</li>  
    <li>last name: ${lastName}</li>  
</ul>
        
    </body>
    </html>`    

}

/**
 * Check if the client accepts html and send a response accordingly.
 * The response will include the first name and last name.
 * @param req 
 * @param res 
 * @param firstName 
 * @param lastName 
 */
 function sendResonseToClient(req : Request ,res :Response,firstName : string,lastName : string){
    if (req.accepts("text/html")){
        const markup = getNameInHTML(firstName,lastName)
        res.status(200).set('Content-Type', 'text/html').send(markup)
 
    }else{
        res.status(200).json({firstName,lastName})
    }
}

//access body 
router.get('/',(req : Request,res :Response)=>{
    sendResonseToClient(req,res,req.body?.firstName,req.body?.lastName)
})

// access query string
router.get('/query',  (req:Request, res : Response) => {
    sendResonseToClient(req,res,req.query?.firstName as string,req.query?.lastName as string)
   
})

//access params
router.get('/params/:firstName/:lastName',(req,res)=>{
    sendResonseToClient(req,res,req.params?.firstName,req.params?.lastName)
})

export default router;
