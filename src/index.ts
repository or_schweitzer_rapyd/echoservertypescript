import express from 'express';
import { Request,Response, NextFunction } from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import router from './Router.js'

const { PORT, HOST = "localhost" } = process.env;


console.log(`port is ${PORT}`)
const app = express();
//const router = express.Router();
app.use('/router', router)
app.use( morgan('dev') );

app.use(express.json())
//router.use(express.json())


app.use((req,res,next) =>{
    console.log("i'm app level middleware function");
    next();
})


function sayHello(req : Request,res :Response,next : NextFunction){
    console.log("hello specific middleware");
    next();
}

app.get('/hello',sayHello,(req : Request,res : Response) =>{
    res.status(200).send('hello');
})



//not found
app.get('*',  (req : Request, res : Response) => {
        res.status(404).send('bad request please try again-- 404 not found---')
})



app.listen(Number(PORT), HOST,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});

